# backend on GCS

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.10.0"
    }
  }
  backend "gcs" {
    bucket = "nl-sandboxes-layer1-tfstate"
    prefix = "terraform/state"
  }
}
